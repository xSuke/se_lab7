import pytest
from ..src.case_handlers import *

def get_path(filename):
    return "/home/osirv/pi_labs/suke/se_lab7/tests/test_assets/" + filename

def test_case_file_exsist_true():
    """Testiraj za putanju koja postoji"""
    path = get_path("file.html")
    handler = CaseFileExistsHandler()
    assert handler.test(path) == True

def test_case_file_exsist_false():
    """Testiraj za putanju koja ne postoji"""
    path = get_path("file_a.html")
    handler = CaseFileExistsHandler()
    assert handler.test(path) == False

def test_case_file_exsist_true_run():
    """testiraj kada file postoji hoce ga ispravno ucitati"""
    path = get_path("file.html")
    handler = CaseFileExistsHandler()
    assert "This file exists" in handler.run(path)

def test_case_file_exsist_false_run():
    """testiraj kada file ne postoji hoce ga ispravno ucitati"""
    path = get_path("file_a.html")
    handler = CaseFileExistsHandler()
    with pytest.raises(CaseError) as error:
        handler.run(path)
    assert error.value.error_code == 500

def tests_case_file_exsist_true():
    path = get_path("file.html")
    handler = CaseFileNotExistsHandler()
    assert handler.test(path) == False

def tests_case_file_exsist_false():
    path = get_path("file_a.html")
    handler = CaseFileNotExistsHandler()
    assert handler.test(path) == True

def tests_case_file_exsist_false_run():
    """testiraj kada file ne postoji hoce ga ispravno ucitati"""
    path = get_path("file_a.html")
    handler = CaseFileNotExistsHandler()
    with pytest.raises(CaseError) as error:
        handler.run(path)
    assert error.value.error_code == 404
	

	#smrdi govno 